# Version : 0.18.0

fix: dockle option

# Version : 0.17.0

fix: trivy options

# Version : 0.16.0

add: commands

# Version : 0.15.0

remove: node

# Version : 0.14.0

add: go-run

# Version : 0.13.0

add: commands

# Version : 0.12.0

fix: ruby-run

# Version : 0.11.0

add: gcloud

# Version : 0.10.0

fix: server command

# Version : 0.9.0

add: ruby-watch

# Version : 0.8.0

fix: sudo

# Version : 0.7.0

add: script

# Version : 0.6.0

fix: elm

# Version : 0.5.0

fix: servername

# Version : 0.4.0

add: kubectl

# Version : 0.3.0

refactor: command name

# Version : 0.2.0

add: commands

# Version : 0.1.1

fix: variables

# Version : 0.1.0

add: gitlab-ci

